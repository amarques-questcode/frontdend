def LABEL_ID = "questcode-${UUID.randomUUID().toString()}"
podTemplate(
    name: 'questcode',
    namespace: 'devops',
    label: 'questcode',
    containers: [
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', name: 'container-docker', ttyEnabled: true, workingDir: '/home/jenkins'),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.13.1', name: 'container-helm', ttyEnabled: true)
    ],
    volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
    ],
)
{
    //Start pipeline
    node(LABEL_ID) {
        def REPOS
        def IMAGE_VERSION
        def IMAGE_POSFIX = ""
        def KUBE_NAMESPACE
        def IMAGE_NAME = "frontend"
        def ENVIRONMENT
        def GIT_REPOS_URL = "git@gitlab.com:amarques-questcode/frontdend.git"
        def GIT_BRANCH
        def HELM_CHART_NAME = "questcode/frontend"
        def HELM_DEPLOY_NAME
        def CHARTMUSEUM_URL = "http://helm-chartmuseum:8080"
        def INGRESS_HOST = "questcode.org"

        stage('Checkout') {
            echo 'Iniciando Clone do Repositorio'

            REPOS = checkout([$class: 'GitSCM', branches: [[name: '*/master'], [name: '*/develop']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: GIT_REPOS_URL]]])

            GIT_BRANCH = REPOS.GIT_BRANCH
            
            if(GIT_BRANCH.equals("origin/master")) {
                KUBE_NAMESPACE = "prod"
                ENVIRONMENT = "production"

            } else if (GIT_BRANCH.equals("origin/develop")) {
                KUBE_NAMESPACE = "staging"
                ENVIRONMENT = "staging"
                IMAGE_POSFIX = "-RC"
                INGRESS_HOST = ENVIRONMENT + "-" + INGRESS_HOST

            } else {
                def errorMessage = echo "Nao existe pipeline para a branch ${GIT_BRANCH}"
                echo errorMessage
                throw new Exception(errorMessage)
            }
            
            HELM_DEPLOY_NAME = KUBE_NAMESPACE + "-frontend"
            IMAGE_VERSION = sh returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim();
        }

        stage('Package') {
            container('container-docker') {
                echo 'Iniciando empacotamento com Docker'
                
                withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'DOCKER_HUB_PASSWORD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh "docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}"
                    sh "docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} . --build-arg NPM_ENV='${ENVIRONMENT}'"
                    sh "docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}"
                }
            }
        }

        stage('Deploy') {
            container('container-helm') {
                echo 'Iniciando Deploy com Helm'

                sh 'helm init --client-only'
                sh "helm repo add questcode ${CHARTMUSEUM_URL}"
                sh 'helm repo update'

                try {
                    sh "helm upgrade --namespace=${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.host[0]=${INGRESS_HOST}"
                } catch(Exception e) {
                    sh "helm install --namespace=${KUBE_NAMESPACE} --name ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.host[0]=${INGRESS_HOST}"
                }
            }
        }
    }
}